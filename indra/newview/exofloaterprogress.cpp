/** 
 * @file exofloaterprogress.cpp
 * @brief The alternative teleport/login progress/loading window
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "llfloaterreg.h"

#include "exofloaterprogress.h"

#include "llagent.h"
#include "llappviewer.h"
#include "llbutton.h"
#include "llprogressbar.h"
#include "llstartup.h"
#include "lltextbox.h"
#include "llweb.h"

exoFloaterProgress* exoFloaterProgress::sInstance = NULL;

exoFloaterProgress::exoFloaterProgress(const LLSD& key) :
	LLFloater(key),
	mPercentDone(0.f)
{
	sInstance = this;
}

BOOL exoFloaterProgress::postBuild()
{
	mProgressBar = getChild<LLProgressBar>("login_progress_bar");

	mCancelBtn = getChild<LLButton>("cancel_btn");
	mCancelBtn->setClickedCallback(boost::bind(exoFloaterProgress::cancel));
	setCloseCallback(boost::bind(exoFloaterProgress::cancel));

	getChild<LLTextBox>("title_text")->setText(LLStringExplicit(LLAppViewer::instance()->getSecondLifeTitle()));

	LLFloater::setVisible(FALSE);

	return TRUE;
}

exoFloaterProgress::~exoFloaterProgress()
{
	sInstance = NULL;
}

exoFloaterProgress* exoFloaterProgress::getInstance()
{
	if (!sInstance) sInstance = LLFloaterReg::getTypedInstance<exoFloaterProgress>("exo_progress");
	return sInstance;
}

void exoFloaterProgress::setText(const std::string& text)
{
	getChild<LLUICtrl>("progress_text")->setValue(text);
}

void exoFloaterProgress::setPercent(const F32 percent)
{
	mProgressBar->setValue(percent);
}

void exoFloaterProgress::setCancelButtonVisible(BOOL b, const std::string& label)
{
	mCancelBtn->setLabelSelected(label);
	mCancelBtn->setLabelUnselected(label);
	mCancelBtn->setEnabled(b);
	mCancelBtn->setVisible(b);
}

void exoFloaterProgress::cancel()
{
	if (LLStartUp::getStartupState() < STATE_STARTED)
	{
		LLAppViewer::instance()->requestQuit();
	}
	else
	{
		gAgent.teleportCancel();
		sInstance->mCancelBtn->setEnabled(FALSE);
		sInstance->setVisible(FALSE);
	}
}

bool exoFloaterProgress::handleUpdate(const LLSD& event_data)
{
	LLSD desc = event_data.get("desc");
	LLSD percent = event_data.get("percent");

	if (desc.isDefined()) setText(desc.asString());
	if (percent.isDefined()) setPercent(percent.asReal());

	return false;
}

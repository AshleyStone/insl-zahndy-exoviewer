/** 
 * @file exofloaterquicktools.h
 * @brief EXOFloaterQuickTools class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_QUICK_TOOLS
#define EXO_FLOATER_QUICK_TOOLS

#include "lltransientdockablefloater.h"

class exoFloaterQuickTools :
	public LLTransientDockableFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterQuickTools(const LLSD& key);
	~exoFloaterQuickTools();

	BOOL postBuild();

	virtual void onOpen(const LLSD& key);

	void onChangeSkyPreset(LLUICtrl* ctrl);

	void onClickSkyPrev();
	void onClickSkyNext();

	void onChangeWaterPreset(LLUICtrl* ctrl);

	void onClickWaterPrev();
	void onClickWaterNext();
};

#endif // EXO_FLOATER_QUICK_TOOLS
